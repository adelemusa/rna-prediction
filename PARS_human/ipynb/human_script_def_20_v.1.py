import Funzioni as fn
import numpy as np
import csv


file_in='../human_files/norm.GM12891.pars.tab'
path='../CSV/'
CSV_name=path+'sliding_windows_13_20.csv'


# In[48]:

print('Creating dict...')
pars_scores=fn.create_dict(file_in,0,1)
normalized_pars_scores=fn.maxmin_normalization(pars_scores)


# In[49]:

print('Importing sequences...')
sequences=fn.import_sequences('../human_files/hg19_transcriptome.fa')


# In[50]:

print('Zip scores, sequences dicts...')
sequences_scored_human=fn.score_sequence_dictionary_zip(sequences , normalized_pars_scores )


# In[52]:
print('Sliding windows generating...')

sliding_windows=fn.win_gen_5(sequences_scored_human,13,20,-20)
#sliding_windows=fn.win_gen_all(sequences_scored_human,13)


# In[53]:


sliding_windows_0=sliding_windows[0]
sliding_windows_1=sliding_windows[1]


# In[54]:


sliding_windows_0=fn.unique(sliding_windows_0)


# In[55]:


len(sliding_windows_0)


# In[56]:


len(sliding_windows_1)


# In[57]:


sliding_windows_1=fn.unique(sliding_windows_1)


# In[58]:
print('Removing clasess\'s duplicates...')
for el in sliding_windows_1:
    if el in sliding_windows_0:
        sliding_windows_0.remove(el)
        sliding_windows_1.remove(el)  


# In[59]:
print('Traslation in tetramer...')

translated_pos_windows=fn.sequences2tetramer(sliding_windows_0)
translated_neg_windows=fn.sequences2tetramer(sliding_windows_1)



#fn.csv_fragments_writer(translated_pos_windows, translated_pos_windows, CSV_name, 'w')


# In[63]:

print('CSV writing...')
import csv

with open(CSV_name,mode='a') as csvfile:
    instance=csv.writer(csvfile,delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for ele in translated_pos_windows:
        ele+=str(1)
        instance.writerow([n for n in ele])
    for el in translated_neg_windows:
        el+=str(0)
        instance.writerow([n for n in el])

