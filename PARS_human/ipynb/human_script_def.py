
# coding: utf-8

# In[212]:


import Funzioni as fn
import numpy as np
import csv
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
get_ipython().run_line_magic('matplotlib', 'inline')


# In[213]:


file_in='../human_files/xaa/xaj'
path='../CSV/'
CSV_name=path+'sliding_windows_13_5.csv'


# In[214]:


pars_scores=fn.create_dict(file_in,0,1)
normalized_pars_scores=fn.maxmin_normalization(pars_scores)


# In[215]:


sequences=fn.import_sequences('../human_files/hg19_transcriptome.fa')


# In[216]:


output=fn.score_sequence_dictionary_zip(sequences , normalized_pars_scores )
sequences_scored_human=output[0]


# In[217]:


sliding_windows=fn.win_gen_5(sequences_scored_human,13)
#sliding_windows=fn.win_gen_all(sequences_scored_human,13)


# In[218]:


sliding_windows_0=sliding_windows[0]
sliding_windows_1=sliding_windows[1]


# In[219]:


sliding_windows_0=fn.unique(sliding_windows_0)


# In[220]:


sliding_windows_1=fn.unique(sliding_windows_1)


# In[221]:


for el in sliding_windows_1:
    if el in sliding_windows_0:
        sliding_windows_0.remove(el)
        sliding_windows_1.remove(el)  


# In[222]:


translated_pos_windows=fn.sequences2tetramer(sliding_windows_0)
translated_neg_windows=fn.sequences2tetramer(sliding_windows_1)


# In[223]:


fn.csv_fragments_writer(translated_pos_windows, translated_pos_windows, CSV_name, 'a')

