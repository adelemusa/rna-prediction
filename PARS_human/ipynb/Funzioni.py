import numpy as np
import itertools as it
from random import randint
import csv



def create_dict(file_name, pos1, pos2, start=0, stop=None):
    with open(file_name) as f:
        scores = dict()
        for element in it.islice(f, start, stop):
            line = element.strip().split('\t')
            if line[pos1].startswith('NM'):
                scores[line[pos1]] = [float(element) for element in (line[pos2].split(';'))]
    return scores
    
    
def maxmin_normalization(dictionary):
    newdict = {}
    for key in dictionary.keys():
        mas = max(dictionary[key])
        mini = min(dictionary[key])
        if not mas-mini==0:
            newdict[key] = [((i - mini) / (mas - mini)) for i in dictionary[key]]         
    return newdict
    
def import_sequences (nome_file):
    file_=open(nome_file).read().split('>')
    sequences={}
    for el in file_[1:]:
        refseq = el.split('\n')[0]
        sequences[refseq]=el.split('\n')[1].strip()
    return sequences
    
def score_sequence_dictionary_zip(seq,scores):
    unique_dictionary = {}
    for key in scores.keys():
        try:
            if len(scores[key]) == len(seq[key]):
                unique_dictionary[key] = [list(couple) for couple in zip(seq[key] , scores[key])] ######mod
            else:
                print('Different length!')
        except Exception as e:
            print(e)
    return (unique_dictionary)
    
def list_replace(li,element,replace,times=1):
    for e in range(times):
        li[li.index(element)]=replace
        
def list_replace_some(li,replace,start1,end1,start2,end2):
    for time in range(start1,end1):
        li[li.index(li[time])]=replace
    for time in range(start2,end2):
        li[li.index(li[time])]=replace
    return li
    
    
def win_gen_5(dictionary, win_length, min_limit=5, max_limit=-5):
    sliding_windows_0 = []
    sliding_windows_1 = []
    nk = 0
    for key, i in dictionary.items(): 
        scores=[el[1] for el in i]
        scores=list_replace_some(scores,0.5,0,6,len(scores)-6,len(scores))
        sorted_string=sorted(scores[6:-6])
        for pi in [el for el in sorted_string[:min_limit] if el<0.3]:
            startp=int(scores.index(pi)-(win_length-1)/2)
            endp=int(scores.index(pi)+(win_length-1)/2)
            sliding_windows_0.append(''.join(el[0] for el in i[startp:endp+1]))
            list_replace(scores,pi,'x')
        for pu in [el for el in sorted_string[max_limit:] if el>0.7]:
            startn=int(scores.index(pu)-(win_length-1)/2)
            endn=int(scores.index(pu)+(win_length-1)/2)
            sliding_windows_1.append(''.join(el[0] for el in i[startn:endn+1]))
            list_replace(scores,pu,'x')
        nk += 1
        print(key+':'+'\t'+str(nk) + '/' + str(len(dictionary)) + '\n')
    return (sliding_windows_0, sliding_windows_1)
    
def win_gen_all(dictionary, win_length):
    sliding_windows_0 = []
    sliding_windows_1 = []
    nk = 0
    for key, i in dictionary.items():
        n = 0
        while n <= len(i) - win_length:
            if i[n + int(win_length / 2 )][1] > 0.8:
                sliding_windows_1.append(''.join(el[0] for el in i[n:win_length + n]))   ####cambiato
            if i[n + int(win_length / 2 )][1] < 0.2:
                sliding_windows_0.append(''.join(el[0] for el in i[n:win_length + n]))
            n += 1
        nk += 1
        print(key+':'+str(nk) + '/' + str(len(dictionary)) + '\n')
    return (sliding_windows_0, sliding_windows_1)    
    
def sequences2tetramer(sequences):
    nucleotides = {'A': '0001', 'G': '0010', 'T': '0100', 'C': '1000'}
    translated_windows = []
    for el in sequences:
        single_window = ''
        for base in el:
            single_window += nucleotides[base]
        translated_windows.append(single_window)
    return translated_windows
 
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]
    
def csv_fragments_writer (translated_pos_windows, translated_neg_windows, CSV_name, mod):
    with open(CSV_name,mode='a') as csvfile:
        instance=csv.writer(csvfile,delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for ele in translated_pos_windows:
            ele+=str(1)
            instance.writerow([n for n in ele])
        for el in translated_neg_windows:
            el+=str(0)
            instance.writerow([n for n in el])